var index = 0;
var dataLength = 0;
var slideData;

$.getJSON("./data.json", function(data) { //get the json data
	slideData = data;	//put it into a global variable so that it can be accessed later
	$("#title").text(data.title);
	var imagePath = "url(img/" + data.slides[0].image_file_name + ")" ;
	$("#image").css("content", imagePath);
	$("#sup").text(data.slides[0].caption);
	dataLength = data.slides.length;
});

function update1() {	//if left button is clicked
	if (index != 0)
		index--;
	var imagePath = "url(img/" + slideData.slides[index].image_file_name + ")" ;
	$("#image").css("content", imagePath);
	$("#sup").text(slideData.slides[index].caption);
}

function update2() {	//if right button is clicked
	if (index != (dataLength - 1))
		index++;
	var imagePath = "url(img/" + slideData.slides[index].image_file_name + ")" ;
	$("#image").css("content", imagePath);
	$("#sup").text(slideData.slides[index].caption);
}

function popup() {
	var x = document.getElementById("sup");
	x.style.color = randomColors();
}

function randomColors() {
	return '#' + Math.floor(Math.random() * 16777215).toString(16);
}

function change() {
	var buttonText = document.getElementById("buttonm");
	if (buttonText.innerHTML.match("Play")) {		
		buttonText.innerHTML = "Pause";
		playSlideShow = setInterval(function() {myTimer() }, 3000);
	}
	else {
		buttonText.innerHTML = "Play";	
		clearInterval(playSlideShow);
	}
}

function myTimer() {
	if (index == dataLength)
		index = 0;
	else {
		index++;
		var imagePath = "url(img/" + slideData.slides[index].image_file_name + ")" ;
		$("#image").css("content", imagePath);
		$("#sup").text(slideData.slides[index].caption);
	}
}
