package ssm.controller;

import java.util.Collections;
import properties_manager.PropertiesManager;
import static ssm.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static ssm.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import ssm.model.SlideShowModel;
import ssm.model.Slide;
import javafx.scene.layout.VBox;
import ssm.view.SlideShowMakerView;
import ssm.view.SlideEditView;

/**
 * This controller provides responses for the slideshow edit toolbar,
 * which allows the user to add, remove, and reorder slides.
 * 
 * @author McKilla Gorilla & _______Raymond Lam_
 */
public class SlideShowEditController {
    // APP UI
    private SlideShowMakerView ui;
    public String caption = "";
    
    /**
     * This constructor keeps the UI for later.
     */
    public SlideShowEditController(SlideShowMakerView initUI) {
	ui = initUI;
    }
    
    /**
     * Provides a response for when the user wishes to add a new
     * slide to the slide show.
     */
    public void processAddSlideRequest() {
	SlideShowModel slideShow = ui.getSlideShow();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	slideShow.addSlide(DEFAULT_SLIDE_IMAGE, PATH_SLIDE_SHOW_IMAGES, caption);
        ui.reloadSlideShowPane(slideShow);
        ui.updatePreviewButton();
        ui.updateSaveButton();

    }
    
    public void processRemoveSlideRequest(Slide toRemove) {
        SlideShowModel slideShow = ui.getSlideShow();
        ui.getSlideShow().getSlides().remove(ui.getSlideShow().getSelectedSlide());
        ui.slideShow.setSelectedSlide(null);
        ui.reloadSlideShowPane(slideShow);
    }
    
    public void processMoveUpRequest(Slide toMove) {
        SlideShowModel slideShow = ui.getSlideShow();  
        int origin = slideShow.getSlides().indexOf(slideShow.getSelectedSlide());
        int dest = origin-1;
        Slide temp2 = slideShow.getSlides().get(dest);
        Slide temp = slideShow.getSlides().get(origin);
        slideShow.swap(temp, temp2, origin, dest);     
        ui.reloadSlideShowPane(slideShow);  
        
    }
    
    public void processMoveDownRequest(Slide toMove) {
        SlideShowModel slideShow = ui.getSlideShow();     
        int origin = slideShow.getSlides().indexOf(slideShow.getSelectedSlide());
        int size = slideShow.getSlides().size();
        int dest = origin+1;
        Slide temp2 = slideShow.getSlides().get(dest);
        Slide temp = slideShow.getSlides().get(origin);
        slideShow.swap(temp, temp2, origin, dest);      
        ui.reloadSlideShowPane(slideShow);          
    }
}
