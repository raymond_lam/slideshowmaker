package ssm.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.FileChooser;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.ERROR_CORRUPT_SAVE;
import static ssm.LanguagePropertyType.SAVE_BUTTON_1;
import static ssm.LanguagePropertyType.SAVE_BUTTON_2;
import static ssm.LanguagePropertyType.SAVE_BUTTON_3;
import static ssm.LanguagePropertyType.SAVE_PROMPT_1;
import static ssm.LanguagePropertyType.SAVE_PROMPT_2;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import static ssm.LanguagePropertyType.TOOLTIP_TITLE;
import ssm.SlideShowMaker;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.model.Slide;
/**
 * This class serves as the controller for all file toolbar operations,
 * driving the loading and saving of slide shows, among other things.
 * 
 * @author McKilla Gorilla & _Raymond Lam__
 */
public class FileController {

    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;

    // THE APP UI
    private static SlideShowMakerView ui;
    
    // THIS GUY KNOWS HOW TO READ AND WRITE SLIDE SHOW DATA
    private SlideShowFileManager slideShowIO;
    
    ImageView imageSelectionView;
    BorderPane view;
    int counter = 0;
    Label captionText = new Label("");  
    BorderPane bottom;
    boolean saveWork;
    boolean cancelValue;
    WebView view2;
    WebEngine engine;
    public static String JSON_IMAGE_FILE_NAME = "image_file_name";
    public static String JSON_CAPTION = "caption";
    public static String JSON_SLIDES = "slides";
    public static String JSON_TITLE = "title";
    /**
     * This default constructor starts the program without a slide show file being
     * edited.
     *
     * @param initSlideShowIO The object that will be reading and writing slide show
     * data.
     */
    public FileController(SlideShowMakerView initUI, SlideShowFileManager initSlideShowIO) {
        // NOTHING YET
        saved = true;
	ui = initUI;
        slideShowIO = initSlideShowIO;
    }
    
    public void markAsEdited() {
        saved = false;
        ui.updateToolbarControls(saved);
    }
    
    //deletes everything given a directory; since mkdir doesn't overwrite, need to delete everything first
    private void recursiveDelete(File file){
        if(file.isDirectory())
            for(File temp :file.listFiles())
                recursiveDelete(temp);
        file.delete();
    }
    
    //create array of captions and filenames in json format
    private JsonArray slidesJsonJSArray(List<String> imageNameList, List<String> captionList) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (int a = 0; a < imageNameList.size(); a++) {    //size of imageNameList and captionList will be the same so OK
            JsonObject jso = slideJsonJS(imageNameList.get(a), captionList.get(a));
            jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    //creates the json to give to the javascript file
    private JsonObject slideJsonJS(String imageName, String caption) {
        JsonObject jso = Json.createObjectBuilder()
                            .add(JSON_IMAGE_FILE_NAME, imageName)
                            .add(JSON_CAPTION, caption)
                .build();
        return jso;
    }
    
    public void handleViewRequest() {
        //first try to make the new directory for the slideshow
        //File directory = new File(ui.getTitle().getCharacters().toString());
        File f = new File("./././page.html");   
        File f2 = new File("./././page.css");
        File f3 = new File("./././page.js");
        String filePath = "./././sites/";
        filePath = filePath + ui.getSlideShow().getTitle();
        //first get the current slides to figure out what images are being used
        ObservableList<Slide> slides = ui.getSlideShow().getSlides();
        //then add the filenames of each image the slideshow uses to an arraylist
        ArrayList<String> fileNames = new ArrayList();
        ArrayList<String> filePaths = new ArrayList();
        ArrayList<String> captionList = new ArrayList();
        for(Slide a : slides) {
            //if (fileNames.indexOf(a.getImageFileName()) == -1) {    //if the filename isn't already in this arraylist, then add it
                fileNames.add(a.getImageFileName());               
            //}
            //if (filePaths.indexOf(a.getImagePath() + "/" + a.getImageFileName()) == -1) {   //if the full path isn't already in this arraylist, then add it
                filePaths.add(a.getImagePath() + "/" + a.getImageFileName());
            //}
                captionList.add(a.getCaption());
        }
        //now create the json file (contains only the filename and caption). can't use the saved json as it might not have been saved yet
        String title = ui.getSlideShow().getTitle();        
        //now we know what images are being used by the current slideshow; now we can add the appropriate ones to the img directory
        File file = new File(filePath);   
        if(!file.mkdir())   //if the directory isn't created, then create it and add new files. mkdir fails if it already exists, so need to delete files first in that case
            recursiveDelete(file);
        file.mkdir();
        String filePath2 = filePath + "/css/";
        String filePath3 = filePath + "/js/";
        String filePath4 = filePath + "/img/";
        new File(filePath2).mkdir();
        new File(filePath3).mkdir();
        new File(filePath4).mkdir();
        String filePath5 = filePath + "/page.html"; //prepare html file path
        String filePath6 = filePath + "/css/page.css";  //prepare css file path
        String filePath7 = filePath + "/js/page.js";    //prepare js file path
        try {
            Files.copy(f.toPath(), new File(filePath5).toPath());   //copy the html file
            Files.copy(f2.toPath(), new File(filePath6).toPath());  //copy the css file
            Files.copy(f3.toPath(), new File(filePath7).toPath());  //copy the js file
            for (int a = 0; a < fileNames.size(); a++) {    //now add the img files; these fail if the file already exists, so works
                Files.copy(new File(filePaths.get(a)).toPath(), new File(filePath4 + fileNames.get(a)).toPath());
            }
            String jsonFilePath = filePath + "/data.json";  //create a new json file in the directory whenever you view
            OutputStream os = new FileOutputStream(jsonFilePath);
            JsonWriter jsonWriter = Json.createWriter(os);
            JsonArray toGiveToJs = slidesJsonJSArray(fileNames, captionList);
            JsonObject courseJsonObject = Json.createObjectBuilder()
                                            .add(JSON_TITLE, title)
                                            .add(JSON_SLIDES, toGiveToJs)
                    .build();
            jsonWriter.writeObject(courseJsonObject);
        }
        catch (Exception e) {   //error handling for later
            
        }
        
        ui.getWindow().hide();
        Stage stage = new Stage();
        stage.setOnCloseRequest((WindowEvent we) -> {
            we.consume();
            ui.getWindow().show();
            stage.close();
        });        
        view2 = new WebView();
        engine = view2.getEngine();
        view2.setPrefSize(1200, 800);
        File f4 = new File(filePath + "/page.html");
        try {
            engine.load(f4.toURI().toURL().toString());
        }
        catch (Exception e) {
            //for error handling later
        }
        stage.resizableProperty().set(false);
        Scene scene = new Scene(new Group());
        scene.setRoot(view2);
        stage.setScene(scene);
        stage.getIcons().add(new Image("file:icon.png"));
        stage.setTitle(SlideShowMaker.appTitle + " - " + ui.getSlideShow().getTitle());
        stage.show();
    }
    
  /*  public void handleViewRequest() {   
        ui.getWindow().hide();
        Stage stage = new Stage();       
        stage.setOnCloseRequest((WindowEvent we) -> {
           we.consume();
           ui.getWindow().show();
           stage.close();
        });
        view = new BorderPane();
        stage.resizableProperty().set(false);
        Scene scene = new Scene(view, 1200, 800);
        stage.setScene(scene);
        stage.getIcons().add(new Image("file:icon.png"));
        stage.setTitle(SlideShowMaker.appTitle + " - " + ui.getSlideShow().getTitle());
        imageSelectionView = new ImageView();
        imageSelectionView.setFitHeight(500);
        imageSelectionView.setFitWidth(500);
        imageSelectionView.setPreserveRatio(true);
        ObservableList<Slide> slides = ui.getSlideShow().getSlides();
        String imagePath = slides.get(0).getImagePath() + SLASH + slides.get(0).getImageFileName();
        File file = new File(imagePath);
        try {
            URL fileURL = file.toURI().toURL();
            Image slideImage = new Image(fileURL.toExternalForm());
            imageSelectionView.setImage(slideImage);
        } catch (Exception e) {
            
        }
        Button leftC = new Button("Left");
        leftC.setOnMouseClicked(e -> {
            if (counter != 0) {
                counter--;
                updateView();
            }
        });
        Button rightC = new Button("Right");    
        rightC.setOnMouseClicked(e -> {
            if (counter != ui.getSlideShow().getSlides().size() - 1) {
                counter++;
                updateView();
            }
        });
        BorderPane right = new BorderPane();
        BorderPane left = new BorderPane();
        bottom = new BorderPane();
        captionText.setText(slides.get(0).getCaption());
        bottom.setCenter(captionText);
        view.setBottom(bottom);
        left.setCenter(leftC);    
        right.setCenter(rightC);
        view.setLeft(left);
        view.setRight(right);
        view.setCenter(imageSelectionView);
        stage.show();
    }
    */
    public void getImage(ObservableList<Slide> slides, int index) {
        String imagePath = slides.get(index).getImagePath() + SLASH + slides.get(index).getImageFileName();
        File file = new File(imagePath);
        try {
            URL fileURL = file.toURI().toURL();
            Image slideImage = new Image(fileURL.toExternalForm());
            imageSelectionView.setImage(slideImage);
            imageSelectionView.setFitWidth(500);
            imageSelectionView.setFitHeight(500);
            imageSelectionView.setPreserveRatio(true);
            captionText.setText(slides.get(index).getCaption());
            bottom.setCenter(captionText);
            view.setBottom(bottom);
        } catch (Exception e) {}
    }
    
    public void updateView() {
        if (counter < 0 || counter >= ui.getSlideShow().getSlides().size()) {
            if (counter < 0)
                counter = 0;
            else
                counter = ui.getSlideShow().getSlides().size();
        }
        else {
            getImage(ui.getSlideShow().getSlides(), counter);
        }
    }
    
    //if the enter key is pressed while in the text field, change the slide show's title
    public void handleChangeTitle(KeyEvent ke) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (ke.getCode().equals(KeyCode.ENTER)) {
            TextField title = ui.getTitle();
            String data = title.getText();
            Label displayTitle = ui.getDisplayTitle();
            displayTitle.setText(data);
            title.setEditable(false);
            title.setText(props.getProperty(TOOLTIP_TITLE.toString()));
            SlideShowModel slideshow = ui.getSlideShow();
            slideshow.setTitle(data);
            ke.consume();
        } 
    }
    
    //changes the caption, static method allows it to be called from slideeditview
    //without said class having an instance of it
    public static void handleChangeCaption(Slide a, String initCaption) {        
        a.setCaption(initCaption);
    }
    
    /**
     * This method starts the process of editing a new slide show. If a pose is
     * already being edited, it will prompt the user to save it first.
     */
    public void handleNewSlideShowRequest() {
        boolean continueToMakeNew;
        try {
            if (ui.getSlideShow().getTitle() == null) {
                continueToMakeNew = true;
            }
            else {
            // WE MAY HAVE TO SAVE CURRENT WORK
                continueToMakeNew = promptToSave();
            }
            //if (promptToSave()) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
           //     continueToMakeNew = promptToSave();
            //}

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                SlideShowModel slideShow = ui.getSlideShow();
		slideShow.reset();
                saved = false;

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                ui.updateToolbarControls(saved);
                ui.reloadSlideShowPane(ui.getSlideShow());
                ui.updatePreviewButton();
                ui.updateMoveButtons();
                ui.updateRemoveButton();
                // TELL THE USER THE SLIDE SHOW HAS BEEN CREATED
                // @todo
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            // @todo provide error message
        }
    }
    

    /**
     * This method lets the user open a slideshow saved to a file. It will also
     * make sure data for the current slideshow is not lost.
     */
    public void handleLoadSlideShowRequest() {
        try {
            boolean continueToOpen;
            if (ui.getSlideShow().getTitle() == null) {
                continueToOpen = true;
            }
            else {
            // WE MAY HAVE TO SAVE CURRENT WORK
            continueToOpen = promptToSave();
            }
            //if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
            //    continueToOpen = promptToSave();
           // }

            // IF THE USER REALLY WANTS TO OPEN A POSE
            if (continueToOpen) {
                // GO AHEAD AND PROCEED MAKING A NEW POSE
                promptToOpen();
                ui.updatePreviewButton();
                SlideShowMakerView.moveUpButton.setDisable(true);
                SlideShowMakerView.moveDownButton.setDisable(true);
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            //@todo provide error message
        }
        
    }

    /**
     * This method will save the current slideshow to a file. Note that we already
     * know the name of the file, so we won't need to prompt the user.
     */
    public boolean handleSaveSlideShowRequest() {
        try {
            if (ui.getSlideShow().getTitle() == null) {
                return false;
            }
	    // GET THE SLIDE SHOW TO SAVE
	    SlideShowModel slideShowToSave = ui.getSlideShow();
	    
            // SAVE IT TO A FILE
            slideShowIO.saveSlideShow(slideShowToSave);

            // MARK IT AS SAVED
            saved = true;

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            ui.updateToolbarControls(saved);
	    return true;
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            // @todo
	    return false;
        }      
    }

     /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     */
    public void handleExitRequest() {
        try {
            boolean continueToExit;
            if (ui.getSlideShow().getTitle() == null) {
                continueToExit = true;
            }
            // WE MAY HAVE TO SAVE CURRENT WORK
            //boolean continueToExit = true;
            //if (!saved) {
                // THE USER CAN OPT OUT HERE
            else {
                continueToExit = promptToSave();
            }
           // }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            // @todo
        }
    
    }

    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * pose, or opening another pose, or exiting. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is retuned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
    private boolean promptToSave() throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        
        //saveWork = savePrompt(); // @todo change this to prompt        
        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (savePrompt() /*&& (ui.getSlideShow().getSlides().size() != 0)*/) {
            SlideShowModel slideShow = ui.getSlideShow();
            try {
                slideShowIO.saveSlideShow(slideShow);
            }
            catch(Exception e) {
                
            }
            saved = true;
        }
        // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        //if (!(exitConfirmation))
        //    return false;
        else if (cancelValue) {
            return false;
        }
        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
        
    }
    
    private boolean savePrompt() {
        cancelValue = false;
        saveWork = true;
        boolean confirmation = false;
        Alert alert = new Alert(AlertType.CONFIRMATION);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        alert.setTitle(props.getProperty(SAVE_PROMPT_1));
        alert.setContentText(props.getProperty(SAVE_PROMPT_2));
        ButtonType yes = new ButtonType(props.getProperty(SAVE_BUTTON_1));
        ButtonType no = new ButtonType(props.getProperty(SAVE_BUTTON_2));
        ButtonType cancel = new ButtonType(props.getProperty(SAVE_BUTTON_3));
        alert.getButtonTypes().setAll(yes, no, cancel);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == yes) {
            confirmation = true;
        }
        else if (result.get() == no) {
            ;
        }
        else {
            cancelValue = true;
        }
        //else {
         //   saveWork = false;            
       // }
        return confirmation;
    }

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen() {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser slideShowFileChooser = new FileChooser();
        slideShowFileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOWS));
        File selectedFile = slideShowFileChooser.showOpenDialog(ui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
		SlideShowModel slideShowToLoad = ui.getSlideShow();
                slideShowIO.loadSlideShow(slideShowToLoad, selectedFile.getAbsolutePath());
                ui.reloadSlideShowPane(slideShowToLoad);
                saved = true;
                ui.updateToolbarControls(saved);
                ui.setDisplayTitle(slideShowToLoad.getTitle());
            } catch (Exception e) {
                ErrorHandler eH = ui.getErrorHandler();
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                String errorMessage = props.getProperty(ERROR_CORRUPT_SAVE);
                eH.processError(ERROR_CORRUPT_SAVE, "Error", errorMessage);
            }
        }
    }

    /**
     * This mutator method marks the file as not saved, which means that when
     * the user wants to do a file-type operation, we should prompt the user to
     * save current work first. Note that this method should be called any time
     * the pose is changed in some way.
     */
    public void markFileAsNotSaved() {
        saved = false;
    }

    /**
     * Accessor method for checking to see if the current pose has been saved
     * since it was last editing. If the current file matches the pose data,
     * we'll return true, otherwise false.
     *
     * @return true if the current pose is saved to the file, false otherwise.
     */
    public boolean isSaved() {
        return saved;
    }
}

