package ssm.view;

import javafx.collections.ObservableList;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.scene.input.KeyEvent;
import javafx.scene.Node;
import javafx.geometry.Pos;
import javafx.scene.input.KeyCode;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.ERROR_IMAGE_UPDATE;
import static ssm.LanguagePropertyType.TOOLTIP_ADD_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_REMOVE_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_UP;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_DOWN;
import static ssm.LanguagePropertyType.TOOLTIP_EXIT;
import static ssm.LanguagePropertyType.TOOLTIP_LOAD_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_NEW_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_SAVE_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_VIEW_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_TITLE;
import static ssm.LanguagePropertyType.TOOLTIP_TITLE_2;
import ssm.StartupConstants;
import static ssm.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_SHOW_EDIT_VBOX;
import static ssm.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.ICON_ADD_SLIDE;
import static ssm.StartupConstants.ICON_REMOVE_SLIDE;
import static ssm.StartupConstants.ICON_MOVE_UP;
import static ssm.StartupConstants.ICON_MOVE_DOWN;
import static ssm.StartupConstants.ICON_EXIT;
import static ssm.StartupConstants.ICON_LOAD_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_NEW_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_SAVE_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_VIEW_SLIDE_SHOW;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import ssm.controller.FileController;
import ssm.controller.SlideShowEditController;
import ssm.model.Slide;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;

/**
 * This class provides the User Interface for this application,
 * providing controls and the entry points for creating, loading, 
 * saving, editing, and viewing slide shows.
 * 
 * @author McKilla Gorilla & _____Raymond Lam____
 */
public class SlideShowMakerView {

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ssmPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newSlideShowButton;
    Button loadSlideShowButton;
    public static Button saveSlideShowButton;
    public static Button viewSlideShowButton;
    Button exitButton;
    
    // text field for title, label to show title
    TextField title;
    Label displayTitle;
    
    //text
    String data;
    
    // WORKSPACE
    HBox workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox slideEditToolbar;
    Button addSlideButton;
    static Button removeSlideButton;
    public static Button moveUpButton;
    public static Button moveDownButton;
    
    // AND THIS WILL GO IN THE CENTER
    ScrollPane slidesEditorScrollPane;
    static VBox slidesEditorPane;

    // THIS IS THE SLIDE SHOW WE'RE WORKING WITH
    public static SlideShowModel slideShow;

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    SlideShowFileManager fileManager;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private static ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;
    
    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private SlideShowEditController editController;

    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public SlideShowMakerView(SlideShowFileManager initFileManager) {
	// FIRST HOLD ONTO THE FILE MANAGER
	fileManager = initFileManager;
	
	// MAKE THE DATA MANAGING MODEL
	slideShow = new SlideShowModel(this);

	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	errorHandler = new ErrorHandler(this);
    }

    // ACCESSOR METHODS
    public SlideShowModel getSlideShow() {
	return slideShow;
    }

    public Stage getWindow() {
	return primaryStage;
    }
    
    public VBox getSlidesEditorPane() {
        return slidesEditorPane;
    }

    public ErrorHandler getErrorHandler() {
	return errorHandler;
    }
    
    public TextField getTitle() {
        return title;
    }
    
    public void setDisplayTitle(String initString) {
        displayTitle.setText(initString);
    }
    
    public Label getDisplayTitle() {
        return displayTitle;
    }
    /**
     * Initializes the UI controls and gets it rolling.
     * 
     * @param initPrimaryStage The window for this application.
     * 
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle) {
	// THE TOOLBAR ALONG THE NORTH
	initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace();

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
	initWindow(windowTitle);
    }

    // UI SETUP HELPER METHODS
    private void initWorkspace() {
	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new HBox();
        workspace.getStyleClass().add(StartupConstants.CSS_ROOT);
        //workspace.getStylesheets().add(CSS_ROOT);
	
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
	slideEditToolbar = new VBox(8);
        slideEditToolbar.setAlignment(Pos.CENTER);
	slideEditToolbar.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_EDIT_VBOX);
	addSlideButton = this.initChildButton(slideEditToolbar,		ICON_ADD_SLIDE,	    TOOLTIP_ADD_SLIDE,	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        removeSlideButton = this.initChildButton(slideEditToolbar, ICON_REMOVE_SLIDE, TOOLTIP_REMOVE_SLIDE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
        moveUpButton = this.initChildButton(slideEditToolbar, ICON_MOVE_UP, TOOLTIP_MOVE_UP, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
        moveDownButton = this.initChildButton(slideEditToolbar, ICON_MOVE_DOWN, TOOLTIP_MOVE_DOWN, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON, true);
	// AND THIS WILL GO IN THE CENTER
	slidesEditorPane = new VBox(20);
        VBox rootPane = new VBox();
        workspace.getStyleClass().add(StartupConstants.CSS_ROOT);
        slidesEditorPane.getStyleClass().add(StartupConstants.CSS_ROOT);
               
	slidesEditorScrollPane = new ScrollPane(slidesEditorPane);
       // slidesEditorScrollPane.getStyleClass().add(StartupConstants.CSS_ROOT2);
      //  slidesEditorPane.getStyleClass().add(CSS_ROOT);
	// NOW PUT THESE TWO IN THE WORKSPACE
        //workspace.setAlignment(Pos.CENTER);
        rootPane.getChildren().add(slidesEditorScrollPane);
	workspace.getChildren().add(slideEditToolbar);
	workspace.getChildren().add(rootPane);
    }

    private void initEventHandlers() {
	// FIRST THE FILE CONTROLS
	fileController = new FileController(this, fileManager);
	newSlideShowButton.setOnAction(e -> {
	    fileController.handleNewSlideShowRequest();
	});
	loadSlideShowButton.setOnAction(e -> {
	    fileController.handleLoadSlideShowRequest();
	});
	saveSlideShowButton.setOnAction(e -> {
	    fileController.handleSaveSlideShowRequest();
	});
        viewSlideShowButton.setOnAction( e-> {
            fileController.handleViewRequest();
        });
	exitButton.setOnAction(e -> {
	    fileController.handleExitRequest();
	});    
	title.setOnMouseClicked(e -> {
            //boolean editable = title.isEditable();
            title.setEditable(true);            
            title.clear();
        });
        title.setOnKeyPressed((KeyEvent ke) -> {
            fileController.handleChangeTitle(ke);
            if (ke.getCode().equals(KeyCode.ENTER)) {
                updateSaveButton();
            }
            /*if (ke.getCode().equals(KeyCode.ENTER)) {
                data = title.getText();
                displayTitle.setText(data);
                title.setEditable(false);
                ke.consume();
            title.setOnKeyPressed((KeyEvent ke) -> {
            if (ke.getCode().equals(KeyCode.ENTER)) {
            }                    */
        });
	// THEN THE SLIDE SHOW EDIT CONTROLS
	editController = new SlideShowEditController(this);
	addSlideButton.setOnAction(e -> {
	    editController.processAddSlideRequest();
            removeSlideButton.setDisable(false);
            if (slidesEditorPane.getChildren().size() == 1) {
                    moveUpButton.setDisable(true);
                    moveDownButton.setDisable(true);
            }
            else if (slidesEditorPane.getChildren().size() > 1) {
                    moveUpButton.setDisable(false);
                    moveDownButton.setDisable(false);
                }
            removeSlideButton.setDisable(true);
            moveUpButton.setDisable(true);
            moveDownButton.setDisable(true);
	});
        removeSlideButton.setOnAction(e -> {
            editController.processRemoveSlideRequest(slideShow.getSelectedSlide());
                removeSlideButton.setDisable(true);
                moveUpButton.setDisable(true);
                moveDownButton.setDisable(true);
                updateSaveButton();
        });
        moveUpButton.setOnAction(e -> {
            editController.processMoveUpRequest(slideShow.getSelectedSlide());
            //int selected = slideShow.
            updateMoveButtons();
            for (Node a : slidesEditorPane.getChildren()) {
                if (((SlideEditView) a).slide == (slideShow.getSelectedSlide())) {
                    a.setStyle("-fx-border-color:rgb(255,215,0);"
                        + "-fx-border-width:5px;");
                }
            }
            updateSaveButton();
        });
        moveDownButton.setOnAction(e -> {
            editController.processMoveDownRequest(slideShow.getSelectedSlide());
            updateMoveButtons();
            for (Node a : slidesEditorPane.getChildren()) {
                if (((SlideEditView) a).slide == (slideShow.getSelectedSlide())) {
                    a.setStyle("-fx-border-color:rgb(255,215,0);"
                        + "-fx-border-width:5px;");
                }
            }
            updateSaveButton();
        });
    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
	fileToolbarPane = new FlowPane();
        fileToolbarPane.setHgap(4);

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	newSlideShowButton = initChildButton(fileToolbarPane, ICON_NEW_SLIDE_SHOW,	TOOLTIP_NEW_SLIDE_SHOW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	loadSlideShowButton = initChildButton(fileToolbarPane, ICON_LOAD_SLIDE_SHOW,	TOOLTIP_LOAD_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	saveSlideShowButton = initChildButton(fileToolbarPane, ICON_SAVE_SLIDE_SHOW,	TOOLTIP_SAVE_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	viewSlideShowButton = initChildButton(fileToolbarPane, ICON_VIEW_SLIDE_SHOW,	TOOLTIP_VIEW_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	exitButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        //add textfield title that can be changed by clicking on it. if it is not in focus, it is grayed out
        displayTitle = new Label();
        title = new TextField();
        title.setText(props.getProperty(TOOLTIP_TITLE.toString()));
        displayTitle.setText(props.getProperty(TOOLTIP_TITLE_2.toString()));
        title.setEditable(false);        
        Tooltip tooltip = new Tooltip(props.getProperty(TOOLTIP_TITLE_2.toString()));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(TOOLTIP_TITLE.toString()));
        title.setTooltip(buttonTooltip);
        displayTitle.setTooltip(tooltip);        
        fileToolbarPane.getChildren().add(title);       
        fileToolbarPane.getChildren().add(displayTitle);
    }

    private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
	primaryStage.setTitle(windowTitle);

	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	ssmPane = new BorderPane();
    //   ssmPane.getStyleClass().add(StartupConstants.CSS_ROOT);
	ssmPane.setTop(fileToolbarPane);	
	primaryScene = new Scene(ssmPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);
	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    
    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    LanguagePropertyType tooltip, 
	    String cssClass,
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    
    /**
     * Updates the enabled/disabled status of all toolbar
     * buttons.
     * 
     * @param saved 
     */
    public void updateToolbarControls(boolean saved) {
	// FIRST MAKE SURE THE WORKSPACE IS THERE
	ssmPane.setCenter(workspace);
	
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	saveSlideShowButton.setDisable(saved);
	viewSlideShowButton.setDisable(false);
	
	// AND THE SLIDESHOW EDIT TOOLBAR
	addSlideButton.setDisable(false);
        removeSlideButton.setDisable(true);
    }

    /**
     * Uses the slide show data to reload all the components for
     * slide editing.
     * 
     * @param slideShowToLoad SLide show being reloaded.
     */
    public static void reloadSlideShowPane(SlideShowModel slideShowToLoad) {
	slidesEditorPane.getChildren().clear();
	for (Slide slide : slideShowToLoad.getSlides()) {
	    SlideEditView slideEditor = new SlideEditView(slide);
            slideEditor.setOnMouseClicked(e -> {            
                resetSelection();
                //updateSelect(slide);
                slideShowToLoad.setSelectedSlide(slide);
                slideEditor.setStyle("-fx-border-color:rgb(255, 215, 0);"
                                        + "-fx-border-width:5px;");
                updateMoveButtons();
                updateRemoveButton();
        }); 
	    slidesEditorPane.getChildren().add(slideEditor);
            Slide selected = slideShowToLoad.getSelectedSlide();
        }
        if (slideShowToLoad.getSlides().size() != 0) {
            removeSlideButton.setDisable(false);
        }        
    }
    
    public static void updateSelect(Slide a) {
        slideShow.setSelectedSlide(a);
    }
    
    public static void updateRemoveButton() {
        Slide selected = slideShow.getSelectedSlide();
        ObservableList<Slide> slides = slideShow.getSlides();
        removeSlideButton.setDisable(false);
        if (selected == null) {
            removeSlideButton.setDisable(true);
        }
    }
    
    public static void updatePreviewButton() {
        if (slideShow.getSlides().size() == 0) {
            viewSlideShowButton.setDisable(true);
        }
        else
            viewSlideShowButton.setDisable(false);
    }
    
    public static void updateMoveButtons() {
        Slide selected = slideShow.getSelectedSlide();
        ObservableList<Slide> slides = slideShow.getSlides();
        moveUpButton.setDisable(false);
        moveDownButton.setDisable(false);
        if (slides.size() == 1) {
            moveUpButton.setDisable(true);
            moveDownButton.setDisable(true);
        }
        else if (slides.indexOf(selected) == 0) {
            moveUpButton.setDisable(true);
        }                    
        else if (slides.indexOf(selected) == slides.size() - 1) {
            moveDownButton.setDisable(true);
        }
        if (slides.size() == 0) {
            moveUpButton.setDisable(true);
            moveDownButton.setDisable(true);
        }
    }   
    
    public static void updateSaveButton() {
        saveSlideShowButton.setDisable(false);
    }
    
    public static void resetSelection() {
        for (int a = 0; a < slidesEditorPane.getChildren().size(); a++) {
            SlideEditView slideEditor = (SlideEditView) slidesEditorPane.getChildren().get(a);
            //changes back to original color
            slideEditor.setStyle("-fx-border-color: rgb(41,41,41);");
        }
    }
    
    public static void loadImageError() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String errorMessage = props.getProperty(ERROR_IMAGE_UPDATE);
        errorHandler.processError(ERROR_IMAGE_UPDATE, "Error", errorMessage);
    }
    
}
