package ssm;

import javafx.scene.image.Image;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.control.*; 
import javafx.scene.Scene;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import xml_utilities.InvalidXMLFileFormatException;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.TITLE_WINDOW;
import static ssm.StartupConstants.CSS_ROOT;
import static ssm.StartupConstants.CSS_ROOT2;
import static ssm.StartupConstants.PATH_DATA;
import static ssm.StartupConstants.PROPERTIES_SCHEMA_FILE_NAME;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import static ssm.StartupConstants.UI_PROPERTIES_FILE_NAME;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;

/**
 * SlideShowMaker is a program for making custom image slideshows. It will allow
 * the user to name their slideshow, select images to use, select captions for
 * the images, and the order of appearance for slides.
 *
 * @author McKilla Gorilla & _Raymond Lam__
 */
public class SlideShowMaker extends Application {
    // THIS WILL PERFORM SLIDESHOW READING AND WRITING
    SlideShowFileManager fileManager = new SlideShowFileManager();

    // THIS HAS THE FULL USER INTERFACE AND ONCE IN EVENT
    // HANDLING MODE, BASICALLY IT BECOMES THE FOCAL
    // POINT, RUNNING THE UI AND EVERYTHING ELSE
    SlideShowMakerView ui = new SlideShowMakerView(fileManager);
    
    public static String appTitle;

    @Override
    public void start(Stage primaryStage) throws Exception {
        //adds icon for window
        primaryStage.getIcons().add(new Image("file:icon.png"));
        //primaryStage.initStyle(StageStyle.UTILITY);
        // prompt user for language using combobox
       
        BorderPane window = new BorderPane();
        window.getStylesheets().add(STYLE_SHEET_UI);
        window.getStyleClass().add(CSS_ROOT2);
        Scene prompt = new Scene(window, 200, 75);
        ObservableList<String> options = FXCollections.observableArrayList(
                "English",
                "Spanish"
        );
        //prompt.getStylesheets().add(CSS_ROOT);
        final ComboBox comboBox = new ComboBox(options);        
        Label prompt2 = new Label("Select a language:");
        HBox hb = new HBox(prompt2);
        hb.setAlignment(Pos.CENTER);
        comboBox.setOnAction((e) -> { 
            String sLanguage = comboBox.getSelectionModel().getSelectedItem().toString();
            if (sLanguage.equals("English")) {
                UI_PROPERTIES_FILE_NAME = "properties_EN.xml";
                load(primaryStage);
            }
            else if (sLanguage.equals("Spanish")) {
                UI_PROPERTIES_FILE_NAME = "properties_SP.xml";
                load(primaryStage);
            }
        });   
        window.setCenter(comboBox);
        //prompt2.setAlignment(Pos.TOP_CENTER);
        window.setTop(hb);        
//        Group root = (Group) prompt.getRoot();        
//        root.getChildren().add(window);
        primaryStage.setScene(prompt);
        primaryStage.show();
    }
    
    public void load(Stage primaryStage) {
        // LOAD APP SETTINGS INTO THE GUI AND START IT UP
        boolean success = loadProperties();
        if (success) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            appTitle = props.getProperty(TITLE_WINDOW);

	    // NOW START THE UI IN EVENT HANDLING MODE
	    ui.startUI(primaryStage, appTitle);
	} // THERE WAS A PROBLEM LOADING THE PROPERTIES FILE
	else {
	    // LET THE ERROR HANDLER PROVIDE THE RESPONSE
	    //ErrorHandler errorHandler = ui.getErrorHandler();
            //PropertiesManager props = PropertiesManager.getPropertiesManager();
            //String languageError = props.getProperty(LanguagePropertyType.ERROR_DATA_FILE_LOADING);
	    //errorHandler.processError(LanguagePropertyType.ERROR_DATA_FILE_LOADING, "Loading Properties Error", "");
	    //System.exit(0);
            try {
                start(primaryStage);
            }
            catch(Exception e) {
                
            }
	}
    }     

    /**
     * Loads this application's properties file, which has a number of settings
     * for initializing the user interface.
     * 
     * @return true if the properties file was loaded successfully, false otherwise.
     */
    public boolean loadProperties() {
        try {
            // LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
	    props.loadProperties(UI_PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
       } catch (InvalidXMLFileFormatException ixmlffe) {
            // SOMETHING WENT WRONG INITIALIZING THE XML FILE
            ErrorHandler eH = ui.getErrorHandler();
           // PropertiesManager props = PropertiesManager.getPropertiesManager();
           // String languageError = props.getProperty(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING);
            eH.processError(LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING, "XML Load Error", "There was an error loading the properties for this language. Try a different one!");
              //          try {
             //   start(primaryStage);
           // }
            //catch(Exception e) {
           //     
           // }
            return false;            
        }        
    }

    /**
     * This is where the application starts execution. We'll load the
     * application properties and then use them to build our user interface and
     * start the window in event handling mode. Once in that mode, all code
     * execution will happen in response to user requests.
     *
     * @param args This application does not use any command line arguments.
     */
    public static void main(String[] args) {
	launch(args);
    }
}
